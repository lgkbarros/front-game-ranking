import {Gamer} from './gamer';

export const GAMERS: Gamer[] = [
    {id: '0', firstName: 'Lucas', lastName: 'Pugliese', victories: 20, matches: 32},
    {id: '1', firstName: 'Iury', lastName: 'Henrique', victories: 10, matches: 13},
    {id: '2', firstName: 'Eduardo', lastName: 'Barros', victories: 9, matches: 10},
    {id: '3', firstName: 'Carlos', lastName: 'Edvaldo', victories: 2, matches: 8}
];