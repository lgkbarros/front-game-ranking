export class Gamer {
    id: string;
    firstName: string;
    lastName: string;
    victories: number;
    matches: number;

    constructor (firstName : string, lastName : string, victories : number, matches : number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.victories = victories;
        this.matches = matches;
    }
}