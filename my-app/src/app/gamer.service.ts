import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Gamer } from './gamer';
import { GAMERS } from './mock-gamers';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GamerService {

  baseUrl = 'http://localhost:8080/';
  gamersEndpoint = 'gamers/';

  constructor(private http: HttpClient) { }

  getGamers(): Observable<Gamer[]> {
    return this.http.get<Gamer[]>(this.baseUrl + "gamers")
            .pipe(
              catchError(this.handleError('getGamers', []))
            );
  }

  addGamer(gamer : Gamer): Observable<Gamer> {
    return this.http.post<Gamer>(this.baseUrl + "gamers", gamer, httpOptions)
            .pipe(
              catchError(this.handleError<Gamer>('addGamer'))
            );
  }

  addVictoryAndMatchByOne(gamer : Gamer) : Observable<any> {
    return this.http.put(this.baseUrl + this.gamersEndpoint + gamer.id + "/increaseVictoriesAndMatchesByOne", gamer, httpOptions)
            .pipe(
              catchError(this.handleError('AddVictoryAndMatchByOne', []))
            );
  }

  addMatchByOne(gamer : Gamer) : Observable<any> {
    return this.http.put(this.baseUrl + this.gamersEndpoint + gamer.id + "/increaseMatchesByOne", gamer, httpOptions)
            .pipe(
              catchError(this.handleError('AddMatchByOne', []))
            );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
