import { Component, OnInit } from '@angular/core';
import { GamerService } from './gamer.service';
import { Gamer } from './gamer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Game Ranking';

  gamers: Gamer[];
  selectedGamer: Gamer;

  constructor(private gamerService: GamerService){}

  ngOnInit(): void {
    this.getGamers();
  }

  getGamers(): void {
    this.gamerService.getGamers()
        .subscribe(gamers => this.gamers = gamers);
  }

  addGamer(firstName : string, lastName : string, victories : number, matches : number): void {
    let gamer = new Gamer(firstName, lastName, victories, matches);
    this.gamerService.addGamer(gamer)
        .subscribe(gamer => {
          if(gamer != null) {
            this.getGamers();
          }
        })
  }

  addVictoryAndMatchByOne(gamer: Gamer): void {
    this.gamerService.addVictoryAndMatchByOne(gamer)
        .subscribe(()=>{
          this.getGamers();
        });
  }

  addMatchByOne(gamer: Gamer): void {
    this.gamerService.addMatchByOne(gamer)
        .subscribe(()=>{
          this.getGamers();
        });
  }

  onSelect(gamer: Gamer): void {
    this.selectedGamer = gamer;
  }
}
